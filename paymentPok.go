
type Payment struct {
	ChargeId       string `json:"chargeId"        bson:"chargeId"`
	OrderReference string `json:"orderReference"  bson:"orderReference"`
	Status         string `json:"status"          bson:"status"`

	BusinessInfo  BusinessInfo `json:"businessInfo"    bson:"businessInfo"`
	AmountInCents int          `json:"amountInCents"   bson:"amountInCents"`
	PaymentInfo   PaymentInfo  `json:"paymentInfo"      bson:"paymentInfo"`
	CreatedDate   time.Time    `json:"createdDate"     bson:"createdDate"`
	LastUpdated   time.Time    `json:"lastUpdated"     bson:"lastUpdated"`

	Operations []Operation `json:"operations"      bson:"operations"`
}

type BusinessInfo struct {
	Brand       string `json:"brand"           bson:"brand"`
	Location    string `json:"location"        bson:"location"`
	CustomerId  string `json:"customerId"      bson:"customerId"`
	Requisitor  string `json:"requisitor"      bson:"requisitor"`
	Marketplace bool   `json:"marketplace"     bson:"marketplace"`
}

type PaymentInfo struct {
	Type            string  `json:"type"            bson:"type"`
	Installments    int     `json:"installments"    bson:"installments"`
	InterestRate    float64 `json:"interestRate"    bson:"interestRate"`
	InterestInCents int     `json:"interestInCents" bson:"interestInCents"`
}

type Operation struct {
	Status        string    `json:"status"          bson:"status"`
	AmountInCents int       `json:"amountInCents"   bson:"amountInCents"`
	CreatedDate   time.Time `json:"createdDate"     bson:"createdDate"`
}
func PaymentPok()Payment  {
	return Payment{}
}
